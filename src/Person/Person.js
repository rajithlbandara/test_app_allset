import React from 'react';

const person = (props) => {
return (
        <div> 
                <p>Hiiiii! I am a person my name is {props.name}. And my age is {props.age} </p>
                {props.children} 
        </div>
);
}

export default person;