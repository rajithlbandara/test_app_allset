import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Person from './Person/Person.js';


class App extends Component {

  state = {
    persons: [
      { name: 'Rajith', age: 23},
      { name: 'Shahan', age: 45},
      { name: 'Pissu', age: 12}
    ]
    ,
    otherstate: 'Hingiye jalak'

  }

  switchHandler = () =>{
    this.setState({
      persons:[{ name: 'Lakmal', age: 23},
      { name: 'Shahan', age: 423},
      { name: 'Pissu', age: 6}
    ]
    })
  }

  render() {
    return (
      <div className="App">
        <h1> This is my react App </h1>
        <button onClick={this.switchHandler}> Switch </button>
        <Person name= {this.state.persons[0].name} age={this.state.persons[0].age}/>
        <Person name= {this.state.persons[1].name} age={this.state.persons[1].age}/> 
        <p> {this.state.otherstate} </p>
      </div>  
    );
  }
}

export default App;
